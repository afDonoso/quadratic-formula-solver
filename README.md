# Quadratic Formula Solver

The objective of this project is to solve any quadratic formula using the method proposed by Po-Shen Loh. This is a new method that can solve the roots for any given quadratic formula (even if the roots are imaginary numbers). This new method has also the benefit of being short.
