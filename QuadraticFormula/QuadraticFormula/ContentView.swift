//
//  ContentView.swift
//  QuadraticFormula
//
//  Created by Andres Donoso on 29/12/19.
//  Copyright © 2019 Andres Donoso. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var a: String = ""
    @State var b: String = ""
    @State var c: String = ""
    
    var roots: (String, String) {
        if let valueA = Double(a), let valueB = Double(b), let valueC = Double(c) {
            return QuadraticFormulaSolver.solveQuadraticFormula(a: valueA, b: valueB, c: valueC)
        } else {
            return ("", "")
        }
    }
    
    var factorization: String {
        QuadraticFormulaSolver.showFactorization(of: roots)
    }
    
    func generateRandomNumbers() {
        let valueA = Double.random(in: 1...20)
        let valueB = Double.random(in: 1...20)
        let valueC = Double.random(in: 1...20)
        
        a = String(format: "%.2f", valueA)
        b = String(format: "%.2f", valueB)
        c = String(format: "%.2f", valueC)
    }
    
    var body: some View {
        NavigationView {
            VStack(alignment: .trailing) {
                Button(action: {
                    self.generateRandomNumbers()
                }) {
                    Image(systemName: "shuffle")
                    .padding()
                }
                
                Form {
                    
                    Section(header: Text("Coefficient of x²")) {
                        TextField("0", text: $a)
                            .keyboardType(.numbersAndPunctuation)
                    }
                    
                    Section(header: Text("Coefficient of x")) {
                        TextField("0", text: $b)
                            .keyboardType(.numbersAndPunctuation)
                    }
                    
                    Section(header: Text("Value of c")) {
                        TextField("0", text: $c)
                            .keyboardType(.numbersAndPunctuation)
                    }
                    
                    Section(header: Text("The roots are:")) {
                        Text("\(roots.0)")
                        Text("\(roots.1)")
                    }
                    
                    Section(header: Text("The factorization is:")) {
                        Text("\(factorization)")
                    }
                }
            }
            .navigationBarTitle("Quadratic Formula")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        .previewDevice(PreviewDevice(rawValue: "iPhone 11"))
    }
}
