//
//  File.swift
//  QuadraticFormula
//
//  Created by Andres Donoso on 29/12/19.
//  Copyright © 2019 Andres Donoso. All rights reserved.
//

import Foundation

extension Double {
    func squared() -> Double {
        return self * self
    }
    
    func abs() -> Double {
        return self < 0 ? self * -1 : self
    }
    
    var isInteger: Bool { rounded() == self}
}

class QuadraticFormulaSolver {
    struct Number {
        var value: String
        var isImaginary: Bool
        var completeValue: String {
            isImaginary && value != "i" ? "i" + value: value
        }
    }

    static func solveQuadraticFormula(a: Double, b: Double, c: Double) -> (String, String) {
        //if a != 1, all formula is divided by a.
        let sum = b / a
        let product = c / a
        
        let avg = (sum * -1) / 2
        
        // avg + u, avg - u are all the solutions
        // avg^2 - u^2 = product
        let uSquared = avg.squared() - product
        
        let value: String
        
        if uSquared == -1 {
            //if uSquared is -1, then it is left as i
            value = "i"
        } else {
            value = "\(String(format: "%.2f", uSquared.abs().squareRoot()))"
            
            //value = uSquared.abs().squareRoot().isInteger ? "\(uSquared.abs().squareRoot())" : "√\(uSquared.abs())"
        }
        
        //if uSquared is a negative number, imaginary result is given
        let u = Number(value: value, isImaginary: uSquared < 0)
        
        let roots: (String, String)
        let formattedAvg = String(format: "%.2f", avg)
        
        if let formattedValue = Double(u.completeValue) {
            roots = ("\(String(format: "%.2f", avg - formattedValue))", "\(String(format: "%.2f", avg + formattedValue))")
        } else {
            roots = ("\(formattedAvg) - \(u.completeValue)", "\(formattedAvg) + \(u.completeValue)")
        }
        
        return roots
    }
    
    static func showFactorization(of roots: (String, String)) -> String {
        let factorization: String
        let root1 = roots.0.replacingOccurrences(of: "-", with: "+")
        let root2 = roots.1.replacingOccurrences(of: "+", with: "-")
        
        if roots.0.hasPrefix("-") {
            factorization = "(x + \(root1.dropFirst(1)))(x + \(root2.dropFirst(1)))"
        } else {
            factorization = "(x - \(root1))(x - \(root2))"
        }
        
        return factorization
    }
}
